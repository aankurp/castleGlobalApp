package com.demo.ankurpratik.resturantapp.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.demo.ankurpratik.resturantapp.R;
import com.demo.ankurpratik.resturantapp.RestaurantApplication;
import com.demo.ankurpratik.resturantapp.adapters.BookMarksAdapter;
import com.demo.ankurpratik.resturantapp.entity.BookMarksDO;
import com.demo.ankurpratik.resturantapp.managers.RestaurantManager;
import com.demo.ankurpratik.resturantapp.ui.activity.RestaurantActivity;
import com.demo.ankurpratik.resturantapp.utils.RecycleViewItemClick;
import com.demo.ankurpratik.resturantapp.utils.ResponseListener;
import com.demo.ankurpratik.resturantapp.utils.RestaurantError;
import com.demo.ankurpratik.resturantapp.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * @author ankurpratik on 10/06/17.
 */

public class BookMarksFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, RecycleViewItemClick {
    private static String      TAG = BookMarksFragment.class.getSimpleName();
    @Inject
    public RestaurantManager   restaurantManager;
    private RecyclerView       bookMarksRecyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BookMarksAdapter   bookMarksAdapter;
    private Toolbar            toolbar;
    private TextView           title;
    private RestaurantActivity restaurantActivity;

    public static BookMarksFragment newInstance() {
        BookMarksFragment fragment = new BookMarksFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        restaurantActivity = (RestaurantActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        RestaurantApplication.getInstance().getApplicationComponent().inject(this);
        View view = inflater.inflate(R.layout.resturant_list, container, false);
        bookMarksRecyclerView = (RecyclerView) view.findViewById(R.id.list);
        bookMarksRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        bookMarksAdapter = new BookMarksAdapter(getActivity().getApplicationContext(), this, new ArrayList<BookMarksDO>());
        bookMarksRecyclerView.setAdapter(bookMarksAdapter);
        toolbar = (Toolbar) restaurantActivity.findViewById(R.id.toolbar);
        title = (TextView) restaurantActivity.findViewById(R.id.title_Main);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        title.setText("BookMarks");
        view.findViewById(R.id.details).setVisibility(View.GONE);
        updateViewWithData();
        return view;
    }

    @Override
    public void onRefresh() {
        updateViewWithData();
    }

    private void updateViewWithData() {
        restaurantManager.getBookMarkedRestaurants(new ResponseListener<List<BookMarksDO>>() {
            @Override
            public void onSuccess(List<BookMarksDO> response) {
                swipeRefreshLayout.setRefreshing(false);
                bookMarksAdapter.setData(response);
            }

            @Override
            public void onError(RestaurantError error) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRecyclerItemClick(View view) {
        int position = bookMarksRecyclerView.getChildAdapterPosition(view);
        BookMarksDO bookMarksDO = bookMarksAdapter.getBookMarkDO(position);
        restaurantManager.removeBookMarks(bookMarksDO);
        bookMarksAdapter.removeData(position);
        bookMarksAdapter.notifyItemRemoved(position);
        ToastUtils.showSuccessMessage(getActivity().getApplicationContext(), "  BookMark removed!!  ");
    }
}