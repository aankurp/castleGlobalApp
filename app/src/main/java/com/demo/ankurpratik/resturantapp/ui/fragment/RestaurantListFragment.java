package com.demo.ankurpratik.resturantapp.ui.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.demo.ankurpratik.resturantapp.R;
import com.demo.ankurpratik.resturantapp.RestaurantApplication;
import com.demo.ankurpratik.resturantapp.enums.FragmentType;
import com.demo.ankurpratik.resturantapp.managers.PermissionManager;
import com.demo.ankurpratik.resturantapp.model.Restaurant;
import com.demo.ankurpratik.resturantapp.model.RestaurantResponse;
import com.demo.ankurpratik.resturantapp.ui.activity.RestaurantActivity;
import com.demo.ankurpratik.resturantapp.utils.ResponseListener;
import com.demo.ankurpratik.resturantapp.utils.RestaurantError;
import com.demo.ankurpratik.resturantapp.utils.ToastUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RestaurantListFragment extends BaseFragment {
    private static String      TAG = RestaurantListFragment.class.getSimpleName();
    private RestaurantActivity restaurantActivity;
    private Toolbar            toolbar;
    private TextView           title;

    public static RestaurantListFragment newInstance() {
        RestaurantListFragment fragment = new RestaurantListFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        restaurantActivity = (RestaurantActivity) context;
        RestaurantApplication.getInstance().getApplicationComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.resturant_list, container, false);
        toolbar = (Toolbar) restaurantActivity.findViewById(R.id.toolbar);
        title = (TextView) restaurantActivity.findViewById(R.id.title_Main);
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        title.setText("Restaurants");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void updateViewWithData(String searchText, boolean clearOld) {
        restaurantManager.getRestaurants(searchText, sortType, new ResponseListener<RestaurantResponse>() {
            @Override
            public void onSuccess(RestaurantResponse response) {
                swipeRefreshLayout.setRefreshing(false);
                Map<String, List<Restaurant>> cuisineToRestaurantMap = new HashMap<String, List<Restaurant>>();
                for (Restaurant restaurant : response.getRestaurants()) {
                    String cuisine = restaurant.getRestaurant().getCuisines();
                    if (cuisineToRestaurantMap.get(cuisine) == null) {
                        cuisineToRestaurantMap.put(cuisine, new ArrayList<Restaurant>());
                    }
                    List<Restaurant> r = cuisineToRestaurantMap.get(cuisine);
                    r.add(restaurant);
                    cuisineToRestaurantMap.put(cuisine, r);
                }
                Log.d(TAG, "updateViewWithData " + cuisineToRestaurantMap);
                restaurantAdapter.setData(cuisineToRestaurantMap);
            }

            @Override
            public void onError(RestaurantError error) {
                swipeRefreshLayout.setRefreshing(false);
                if("Permission denied".equals(error.getErrorMessage())){
                    showOfflineInformationDialog();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult "+grantResults);
        switch (requestCode) {
            case PermissionManager.PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean readWriteAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (readWriteAccepted) {
                        ToastUtils.showSuccessMessage(getActivity().getApplicationContext(), "Permission Granted, Work with offline mode as well");
                    } else {
                        ToastUtils.showErrorMessage(getActivity().getApplicationContext(), "Permission Denied, You cannot work with offline mode");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                                showOfflineModePermissionDialog();
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    @Override
    protected void onSearchButtonClicked() {
        Log.d(TAG, "searchbutton clicked");
        searchString = searchAutoCompleteTextView.getText().toString().trim();
        restaurantManager.insertSearchItem(searchString);
        ToastUtils.showSuccessMessage(getActivity().getApplicationContext(), "  Searching for " + searchString + "  ");
        searchAutoCompleteTextView.setText("");
        searchAutoCompleteTextView.dismissDropDown();
        updateViewWithData(searchString, true);
    }

    @Override
    public void decideVisibilityForSearchBar() {
        searchCardView.setVisibility(View.VISIBLE);
    }

    @Override
    public void decideVisibilityForFabIcon() {
        fab.setVisibility(View.VISIBLE);
    }

    @Override
    protected void decideBookMarkVisibility(Menu menu) {
        MenuItem item = menu.findItem(R.id.favourite);
        item.setVisible(true);
    }

    @Override
    protected void onBookMarksClicked() {
        restaurantActivity.setFragment(FragmentType.BOOKMARK);
    }

    @Override
    public void onRecyclerItemClick(View view) {
        int position = resturantRecyclerView.getChildAdapterPosition(view);
        Restaurant restaurant = restaurantAdapter.getRestaurant(position);
        restaurantManager.bookMarkRestaurant(restaurant);
        restaurantAdapter.notifyItemChanged(position);
        ToastUtils.showSuccessMessage(getActivity().getApplicationContext(), "  BookMark added!!  ");
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void showOfflineModePermissionDialog() {
        String message = "You need read write permission to work with offline mode.";
        DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[] { android.Manifest.permission.WRITE_EXTERNAL_STORAGE }, PermissionManager.PERMISSION_REQUEST_CODE);
                }
            }
        };
        new AlertDialog.Builder(restaurantActivity).setMessage(message).setPositiveButton("OK", okListener).setNegativeButton("Cancel", null).create().show();
    }

    private void showOfflineInformationDialog() {
        String message = "Do you want to work with offline mode? ";
        DialogInterface.OnClickListener okListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                showOfflineModePermissionDialog();
            }
        };
        new AlertDialog.Builder(restaurantActivity).setMessage(message).setPositiveButton("OK", okListener).setNegativeButton("Cancel", null).create().show();
    }
}
