package com.demo.ankurpratik.resturantapp.entity;

/**
 * @author ankurpratik on 10/06/17.
 */

public class BookMarksDO {
    private String restaurantId;
    private String thumbUrl;
    private String priceText;
    private String restaurantName;
    private String ratingText;
    private String address;

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getThumbUrl() {
        return thumbUrl;
    }

    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    public String getPriceText() {
        return priceText;
    }

    public void setPriceText(String priceText) {
        this.priceText = priceText;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRatingText() {
        return ratingText;
    }

    public void setRatingText(String ratingText) {
        this.ratingText = ratingText;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "BookMarksDO{" + "restaurantId='" + restaurantId + '\'' + ", thumbUrl='" + thumbUrl + '\'' + ", priceText='" + priceText + '\'' + ", restaurantName='"
                + restaurantName + '\'' + ", ratingText='" + ratingText + '\'' + ", address='" + address + '\'' + '}';
    }
}
