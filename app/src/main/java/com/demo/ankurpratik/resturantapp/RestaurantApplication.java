package com.demo.ankurpratik.resturantapp;

import android.app.Application;
import android.util.Log;

import com.demo.ankurpratik.resturantapp.di.component.DaggerRestaurantComponent;
import com.demo.ankurpratik.resturantapp.di.component.RestaurantComponent;
import com.demo.ankurpratik.resturantapp.di.modules.ApplicationModule;
import com.demo.ankurpratik.resturantapp.di.modules.DaoModule;
import com.demo.ankurpratik.resturantapp.di.modules.DatabaseModule;
import com.demo.ankurpratik.resturantapp.di.modules.GsonModule;
import com.demo.ankurpratik.resturantapp.di.modules.ManagerModule;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RestaurantApplication extends Application {
    private static RestaurantApplication instance;
    private RestaurantComponent          applicationComponent;

    public static synchronized RestaurantApplication getInstance(){
        if(instance == null){
            Log.d("","application is null");
        }
       return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("","application oncreate");
        instance = this;
        applicationComponent = DaggerRestaurantComponent.builder().applicationModule(new ApplicationModule(this)).gsonModule(new GsonModule()).managerModule(
                new ManagerModule(this)).databaseModule(new DatabaseModule(this)).daoModule(new DaoModule(this)).build();
        applicationComponent.inject(this);
    }

    public RestaurantComponent getApplicationComponent() {
        return applicationComponent;
    }

    public RestaurantApplication getResturantApplication(){
        return instance;
    }

}
