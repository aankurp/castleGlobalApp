package com.demo.ankurpratik.resturantapp.utils;

import android.os.Bundle;

import com.demo.ankurpratik.resturantapp.enums.FragmentType;

/**
 * @author ankurpratik on 10/06/17.
 */

public interface FragmentChanger {
    public void changeFragment();
    public void setFragment(FragmentType tag);
    public void setFragment(FragmentType tag, Bundle bundle);
}
