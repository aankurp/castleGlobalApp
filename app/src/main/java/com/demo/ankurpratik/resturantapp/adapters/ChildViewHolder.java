package com.demo.ankurpratik.resturantapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.demo.ankurpratik.resturantapp.R;

/**
 * @author ankurpratik on 10/06/17.
 */

public class ChildViewHolder extends RecyclerView.ViewHolder {

    private TextView  name, id, address, rating, price;
    private ImageView restaurantImage;
    private LinearLayout  bookMarkLayout;

    public ChildViewHolder(View v) {
        super(v);
        name = (TextView) v.findViewById(R.id.name);
        id = (TextView) v.findViewById(R.id.id);
        address = (TextView) v.findViewById(R.id.address);
        rating = (TextView) v.findViewById(R.id.rating);
        price = (TextView) v.findViewById(R.id.price);
        restaurantImage = (ImageView) v.findViewById(R.id.image);
        bookMarkLayout = (LinearLayout) v.findViewById(R.id.bookmark_wrapper);
    }

    public LinearLayout getBookMarkLayout() {
        return bookMarkLayout;
    }

    public void setBookMarkLayout(LinearLayout bookMarkLayout) {
        this.bookMarkLayout = bookMarkLayout;
    }

    public TextView getPrice() {
        return price;
    }

    public void setPrice(TextView price) {
        this.price = price;
    }

    public TextView getName() {
        return name;
    }

    public void setName(TextView name) {
        this.name = name;
    }

    public TextView getId() {
        return id;
    }

    public void setId(TextView id) {
        this.id = id;
    }

    public TextView getAddress() {
        return address;
    }

    public void setAddress(TextView address) {
        this.address = address;
    }

    public TextView getRating() {
        return rating;
    }

    public void setRating(TextView rating) {
        this.rating = rating;
    }

    public ImageView getRestaurantImage() {
        return restaurantImage;
    }

    public void setRestaurantImage(ImageView restaurantImage) {
        this.restaurantImage = restaurantImage;
    }
}
