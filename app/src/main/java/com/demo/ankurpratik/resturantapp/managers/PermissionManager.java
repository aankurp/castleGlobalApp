package com.demo.ankurpratik.resturantapp.managers;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

/**
 * @author ankurpratik on 11/06/17.
 */

public class PermissionManager {
    public static final int PERMISSION_REQUEST_CODE = 200;
    private static String   TAG                     = PermissionManager.class.getSimpleName();
    private Context         context;

    public PermissionManager(Context context) {
        this.context = context;
    }

    public boolean isReadWritePermissionGranted() {
        int result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public void requestReadWritePermissionPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[] { android.Manifest.permission.WRITE_EXTERNAL_STORAGE }, PERMISSION_REQUEST_CODE);
    }
}
