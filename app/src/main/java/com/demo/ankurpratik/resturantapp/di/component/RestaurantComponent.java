package com.demo.ankurpratik.resturantapp.di.component;

import com.demo.ankurpratik.resturantapp.RestaurantApplication;
import com.demo.ankurpratik.resturantapp.adapters.RestaurantAdapter;
import com.demo.ankurpratik.resturantapp.data.cache.FileManager;
import com.demo.ankurpratik.resturantapp.di.modules.ApplicationModule;
import com.demo.ankurpratik.resturantapp.di.modules.DaoModule;
import com.demo.ankurpratik.resturantapp.di.modules.DatabaseModule;
import com.demo.ankurpratik.resturantapp.di.modules.GsonModule;
import com.demo.ankurpratik.resturantapp.di.modules.ManagerModule;
import com.demo.ankurpratik.resturantapp.managers.RestaurantManager;
import com.demo.ankurpratik.resturantapp.ui.fragment.BaseFragment;
import com.demo.ankurpratik.resturantapp.ui.fragment.BookMarksFragment;
import com.demo.ankurpratik.resturantapp.ui.fragment.RestaurantListFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author ankurpratik on 10/06/17.
 */
@Singleton
@Component(modules = { ApplicationModule.class, GsonModule.class, DatabaseModule.class, DaoModule.class, ManagerModule.class})
public interface RestaurantComponent {
    void inject(RestaurantApplication restaurantApplication);

    void inject(RestaurantManager restaurantManager);

    void inject(BookMarksFragment bookMarksFragment);

    void inject(FileManager fileManager);

    void inject(BaseFragment baseFragment);

    void inject(RestaurantAdapter restaurantAdapter);

    void inject(RestaurantListFragment restaurantListFragment);
}