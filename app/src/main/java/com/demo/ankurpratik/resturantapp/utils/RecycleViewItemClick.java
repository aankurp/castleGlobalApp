package com.demo.ankurpratik.resturantapp.utils;

import android.view.View;

/**
 * @author ankurpratik on 10/06/17.
 */

public interface RecycleViewItemClick {
    public void onRecyclerItemClick(View view);
}