package com.demo.ankurpratik.resturantapp.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author ankurpratik on 10/06/17.
 */

@Module
public class GsonModule {
    @Provides
    @Singleton
    public Gson getGson(){
        return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    }
}
