package com.demo.ankurpratik.resturantapp.utils;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RestaurantError {
    private int    networkResponseCode;
    private int    applicationErrorCode;
    private String errorMessage;
    private String errorDescription = "N/A";

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getNetworkResponseCode() {
        return networkResponseCode;
    }

    public void setNetworkResponseCode(int networkResponseCode) {
        this.networkResponseCode = networkResponseCode;
    }

    public int getApplicationErrorCode() {
        return applicationErrorCode;
    }

    public void setApplicationErrorCode(int applicationErrorCode) {
        this.applicationErrorCode = applicationErrorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    @Override
    public String toString() {
        return "RestaurantError{" +
                "networkResponseCode=" + networkResponseCode +
                ", applicationErrorCode=" + applicationErrorCode +
                ", errorMessage='" + errorMessage + '\'' +
                ", errorDescription='" + errorDescription + '\'' +
                '}';
    }
}
