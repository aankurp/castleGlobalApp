package com.demo.ankurpratik.resturantapp.ui.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import com.demo.ankurpratik.resturantapp.R;
import com.demo.ankurpratik.resturantapp.RestaurantApplication;
import com.demo.ankurpratik.resturantapp.enums.SortType;
import com.demo.ankurpratik.resturantapp.adapters.RestaurantAdapter;
import com.demo.ankurpratik.resturantapp.managers.PermissionManager;
import com.demo.ankurpratik.resturantapp.managers.RestaurantManager;
import com.demo.ankurpratik.resturantapp.model.Restaurant;
import com.demo.ankurpratik.resturantapp.utils.PermissionCallback;
import com.demo.ankurpratik.resturantapp.utils.RecycleViewItemClick;
import com.demo.ankurpratik.resturantapp.utils.ResponseListener;
import com.demo.ankurpratik.resturantapp.utils.RestaurantError;
import com.demo.ankurpratik.resturantapp.utils.ToastUtils;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

/**
 * @author ankurpratik on 10/06/17.
 */

public abstract class BaseFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, RecycleViewItemClick {
    public final static int        WRITE_EXTERNAL_STORAGE = 1;
    private static String          TAG                    = BaseFragment.class.getSimpleName();
    public AutoCompleteTextView    searchAutoCompleteTextView;
    @Inject
    public RestaurantManager       restaurantManager;
    protected View                 fragmentView;
    protected String               searchString           = "";
    protected SortType             sortType               = SortType.NONE;
    protected RecyclerView         resturantRecyclerView;
    protected RestaurantAdapter    restaurantAdapter;
    protected SwipeRefreshLayout   swipeRefreshLayout;
    protected FloatingActionButton fab, currencyFilter, ratingFilter;
    protected CardView             searchCardView;
    private ImageView              searchImageView;
    private Boolean                isFabOpen              = false;
    private Animation              fabOpenAnimation, fabCloseAnimation, rotateForwardAnimation, rotateBackwardAnimation;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        RestaurantApplication.getInstance().getApplicationComponent().inject(this);
        setHasOptionsMenu(true);
        resturantRecyclerView = (RecyclerView) fragmentView.findViewById(R.id.list);
        searchCardView = (CardView) fragmentView.findViewById(R.id.details);
        resturantRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        swipeRefreshLayout = (SwipeRefreshLayout) fragmentView.findViewById(R.id.swipe_container);
        swipeRefreshLayout.setOnRefreshListener(this);
        restaurantAdapter = new RestaurantAdapter(getActivity().getApplicationContext(), this, new HashMap<String, List<Restaurant>>());
        resturantRecyclerView.setAdapter(restaurantAdapter);
        searchAutoCompleteTextView = (AutoCompleteTextView) fragmentView.findViewById(R.id.search);
        searchAutoCompleteTextView.setThreshold(0);
        searchAutoCompleteTextView.setOnClickListener(this);
        searchImageView = (ImageView) fragmentView.findViewById(R.id.searchbutton);
        searchImageView.setOnClickListener(this);
        fab = (FloatingActionButton) fragmentView.findViewById(R.id.fab_master);
        currencyFilter = (FloatingActionButton) fragmentView.findViewById(R.id.currency_filter);
        ratingFilter = (FloatingActionButton) fragmentView.findViewById(R.id.rating_filter);
        fabOpenAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_open);
        fabCloseAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.fab_close);
        rotateForwardAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate_forward);
        rotateBackwardAnimation = AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.rotate_backward);
        fab.setOnClickListener(this);
        currencyFilter.setOnClickListener(this);
        ratingFilter.setOnClickListener(this);
        decideVisibilityForFabIcon();
        decideVisibilityForSearchBar();
        updateViewWithData("", true);
        return fragmentView;
    }

    @Override
    public void onRefresh() {
        updateViewWithData(searchString, true);
    }

    public abstract void updateViewWithData(String searchText, boolean clearOld);

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchbutton:
                onSearchButtonClicked();
                break;
            case R.id.search:
                Log.d(TAG, "  search autocomplete clicked  ");
                updateAutoCompleteTextView();
                break;
            case R.id.fab_master:
                animateFAB();
                break;
            case R.id.currency_filter:
                Log.d(TAG, "currency clicked");
                if (sortType.equals(SortType.COST)) {
                    ToastUtils.showInfoMessage(getContext(), "  Cost filter removed !!  ");
                    sortType = SortType.NONE;
                } else {
                    sortType = SortType.COST;
                    ToastUtils.showInfoMessage(getContext(), "  Cost filter applied !!  ");
                }
                animateFAB();
                updateViewWithData(searchString, true);
                break;
            case R.id.rating_filter:
                Log.d(TAG, "Rating clicked");
                if (sortType.equals(SortType.RATING)) {
                    ToastUtils.showInfoMessage(getContext(), "  Rating filter removed !!  ");
                    sortType = SortType.NONE;
                } else {
                    sortType = SortType.RATING;
                    ToastUtils.showInfoMessage(getContext(), "  Rating filter applied !!  ");
                }
                animateFAB();
                updateViewWithData(searchString, true);
                break;
        }
    }

    protected abstract void onSearchButtonClicked();

    private void updateAutoCompleteTextView() {
        restaurantManager.getSearchItems(new ResponseListener<List<String>>() {
            @Override
            public void onSuccess(final List<String> response) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        searchAutoCompleteTextView.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.select_dialog_item, response));
                        searchAutoCompleteTextView.showDropDown();
                    }
                }, 300);
            }

            @Override
            public void onError(RestaurantError error) {

            }
        });
    }

    public void animateFAB() {
        if (isFabOpen) {
            fab.startAnimation(rotateBackwardAnimation);
            currencyFilter.startAnimation(fabCloseAnimation);
            ratingFilter.startAnimation(fabCloseAnimation);
            currencyFilter.setClickable(false);
            ratingFilter.setClickable(false);
            isFabOpen = false;
            Log.d(TAG, "close");
        } else {
            fab.startAnimation(rotateForwardAnimation);
            currencyFilter.startAnimation(fabOpenAnimation);
            ratingFilter.startAnimation(fabOpenAnimation);
            currencyFilter.setClickable(true);
            ratingFilter.setClickable(true);
            setRatingFilterIcon();
            setPriceFilterIcon();
            isFabOpen = true;
            Log.d(TAG, "open");
        }
    }

    private void setPriceFilterIcon() {
        Log.d(TAG, "setPriceFilterIcon " + sortType);
        if (SortType.COST.equals(sortType)) {
            currencyFilter.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.currency_usd));
        } else {
            currencyFilter.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.currency_usd_off));
        }
    }

    private void setRatingFilterIcon() {
        Log.d(TAG, "setRatingFilterIcon " + sortType);
        if (SortType.RATING.equals(sortType)) {
            ratingFilter.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.star));
        } else {
            ratingFilter.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.star_off));
        }
    }

    public abstract void decideVisibilityForSearchBar();

    public abstract void decideVisibilityForFabIcon();

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        decideBookMarkVisibility(menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    protected abstract void decideBookMarkVisibility(Menu menu);

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                break;
            case R.id.favourite:
                onBookMarksClicked();
                break;
            default:
                return false;
        }
        return true;
    }

    protected abstract void onBookMarksClicked();
}