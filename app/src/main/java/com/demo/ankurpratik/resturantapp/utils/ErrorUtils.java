package com.demo.ankurpratik.resturantapp.utils;

import android.content.Context;
import android.util.Log;

import com.android.volley.NoConnectionError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

/**
 * @author ankurpratik on 10/06/17.
 */

public class ErrorUtils {
    private ErrorUtils() {
    }

    public static RestaurantError parseError( Context context, VolleyError error) {
        RestaurantError restaurantError = new RestaurantError();
        if (error instanceof TimeoutError) {
            // TimeOutError
            ToastUtils.showInfoMessage(context.getApplicationContext(), "Connection Timeout!! Please try again later.");
            restaurantError.setNetworkResponseCode(HttpURLConnection.HTTP_CLIENT_TIMEOUT);
            restaurantError.setErrorMessage("Connection Timeout!! Please try again later.");
        } else if (error instanceof NoConnectionError) {
            // NoConnectionError
            //Utils.showToast(context.getApplicationContext(), "No Connection!! Please try again later.");
            restaurantError.setNetworkResponseCode(HttpURLConnection.HTTP_CLIENT_TIMEOUT);
            restaurantError.setErrorMessage("No Connection!! Please try again later.");
        } else {
            // Server error
            boolean errorParsed = false;
            if (error.networkResponse != null) {
                Log.e("ErrorUtils", "VolleyError - {}" + new String(error.networkResponse.data));
                restaurantError.setNetworkResponseCode(error.networkResponse.statusCode);
                try {
                    JSONObject jsonObject = new JSONObject(new String(error.networkResponse.data));
                    restaurantError.setApplicationErrorCode(Integer.parseInt(jsonObject.getString("code")));
                    restaurantError.setErrorMessage(jsonObject.getString("details"));
                    restaurantError.setErrorDescription(jsonObject.getString("message"));
                    errorParsed = true;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (!errorParsed) {
                restaurantError.setApplicationErrorCode(0);
                restaurantError.setErrorMessage("Internal server error");
            }
        }
        return restaurantError;
    }
}
