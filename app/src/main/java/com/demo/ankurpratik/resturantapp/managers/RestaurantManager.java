package com.demo.ankurpratik.resturantapp.managers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.demo.ankurpratik.resturantapp.RestaurantApplication;
import com.demo.ankurpratik.resturantapp.data.dao.BookMarksDao;
import com.demo.ankurpratik.resturantapp.entity.BookMarksDO;
import com.demo.ankurpratik.resturantapp.enums.SortType;
import com.demo.ankurpratik.resturantapp.data.cache.FileManager;
import com.demo.ankurpratik.resturantapp.data.cache.Serializer;
import com.demo.ankurpratik.resturantapp.data.dao.SearchItemDao;
import com.demo.ankurpratik.resturantapp.entity.SearchItemDO;
import com.demo.ankurpratik.resturantapp.model.Restaurant;
import com.demo.ankurpratik.resturantapp.model.RestaurantResponse;
import com.demo.ankurpratik.resturantapp.network.RestaurantNetworkRequestManager;
import com.demo.ankurpratik.resturantapp.utils.NetworkUtils;
import com.demo.ankurpratik.resturantapp.utils.ResponseListener;
import com.demo.ankurpratik.resturantapp.utils.RestaurantError;
import com.demo.ankurpratik.resturantapp.utils.ToastUtils;
import com.demo.ankurpratik.resturantapp.utils.UrlFactory;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RestaurantManager {
    private static String                  TAG = RestaurantManager.class.getSimpleName();
    @Inject
    public RestaurantNetworkRequestManager restaurantNetworkRequestManager;
    @Inject
    public PermissionManager               permissionManager;
    @Inject
    public Gson                            gson;
    @Inject
    public SearchItemDao                   searchItemDao;
    @Inject
    public Serializer                      serializer;
    @Inject
    public FileManager                     fileManager;
    @Inject
    public BookMarksDao                    bookMarksDao;

    private Context                        context;

    public RestaurantManager(Context context) {
        this.context = context;
        RestaurantApplication.getInstance().getApplicationComponent().inject(this);
    }

    public static String getRestaurantCacheFileName() {
        return "rest_.txt";
    }

    public void getRestaurants(String searchText, SortType sortType, ResponseListener callback) {
        if (NetworkUtils.isNetworkAvailable(context)) {
            getRestaurantsViaApis(searchText, sortType, callback);
        } else {
            getCacheResponse(callback);
            ToastUtils.showInfoMessage(context, "Local restaurants fetched!!");
        }
    }

    public void bookMarkRestaurant(Restaurant restaurant) {
        bookMarksDao.insert(restaurant);
    }

    public void getBookMarkedRestaurants(final ResponseListener<List<BookMarksDO>> callback) {
        List<BookMarksDO> bookMarksDOs = bookMarksDao.getAllBookMarks();
        callback.onSuccess(bookMarksDOs);
    }

    public void isRestaurantBookMarked(Restaurant restaurant, ResponseListener<Boolean> callback) {
        boolean result = bookMarksDao.isRestaurantBookMarked(restaurant.getRestaurant().getId());
        callback.onSuccess(result);
    }

    public void getRestaurantsViaApis(String searchText, SortType sortType, final ResponseListener<RestaurantResponse> callback) {
        Map<String, String> stringStringMap = new HashMap<>();
        searchText = searchText.replaceAll("\\s+", "");
        stringStringMap.put("search", searchText);
        if (!SortType.NONE.equals(sortType)) {
            stringStringMap.put("sortType", sortType.name().toLowerCase());
        }
        String url = UrlFactory.getUrl(UrlFactory.SEARCH_ITEM, stringStringMap);
        restaurantNetworkRequestManager.addApiRequest(Request.Method.POST, url, new JSONObject().toString(), new ResponseListener<String>() {
            @Override
            public void onSuccess(String response) {
                if (response != null) {
                    cacheRestaurants(response);
                    RestaurantResponse restaurantResponse = serializer.deserialize(response, RestaurantResponse.class);
                    Log.d(TAG, "restaurantResponse " + restaurantResponse);
                    callback.onSuccess(restaurantResponse);
                } else {
                    callback.onError(new RestaurantError());
                }
            }

            @Override
            public void onError(RestaurantError error) {
                callback.onError(error);
            }
        });
    }

    private void getCacheResponse(final ResponseListener<RestaurantResponse> callback) {
        if (!permissionManager.isReadWritePermissionGranted()) {
            RestaurantError restaurantError = new RestaurantError();
            restaurantError.setErrorMessage("Permission denied");
            callback.onError(restaurantError);
            return;
        }
        String result = FileManager.retrieveFromFile(context, getRestaurantCacheFileName());
        RestaurantResponse response = null;
        if (result != null) {
            response = serializer.deserialize(result, RestaurantResponse.class);
            callback.onSuccess(response);
        } else {
            callback.onError(new RestaurantError());
        }
        Log.d(TAG, "getCacheResponse " + response);
    }

    private void cacheRestaurants(String response) {
        if (permissionManager.isReadWritePermissionGranted()) {
            Log.d(TAG, "cacheRestaurants " + response);
            FileManager.insertToFile(context, response, getRestaurantCacheFileName());
        }
    }

    public void getSearchItems(ResponseListener<List<String>> callback) {
        //todo move it to another thread.
        List<SearchItemDO> searchItems = searchItemDao.getAllSearchItems();
        List<String> items = new ArrayList<>(searchItems.size());
        for (SearchItemDO searchItemDO : searchItems) {
            items.add(searchItemDO.getSearchText());
        }
        Log.d(TAG, "getSearchItems " + items);
        callback.onSuccess(items);
    }

    public void insertSearchItem(String searchText) {
        Log.d(TAG, "insertSearchItem " + searchText);
        searchItemDao.insertOrUpdate(searchText);
    }

    public void removeBookMarks(BookMarksDO bookMarksDO) {
        bookMarksDao.deleteBookMark(bookMarksDO);
    }
}
