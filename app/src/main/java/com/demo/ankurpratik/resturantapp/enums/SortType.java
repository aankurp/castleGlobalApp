package com.demo.ankurpratik.resturantapp.enums;

/**
 * @author ankurpratik on 10/06/17.
 */

public enum SortType {
    NONE,
    COST,
    RATING
}
