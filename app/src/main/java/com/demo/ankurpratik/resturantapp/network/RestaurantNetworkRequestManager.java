package com.demo.ankurpratik.resturantapp.network;

import java.io.UnsupportedEncodingException;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;
import com.demo.ankurpratik.resturantapp.utils.ErrorUtils;
import com.demo.ankurpratik.resturantapp.utils.ResponseListener;
import com.demo.ankurpratik.resturantapp.utils.RestaurantError;

import android.content.Context;
import android.util.Log;

import dagger.Lazy;

/**
 * @author ankurpratik on 10/06/17.
 */
public class RestaurantNetworkRequestManager {
    public static final int      DEFAULT_TIMEOUT_MS = 30000;
    private static String        TAG                = "RestaurantNetworkRequestManager";
    private final Context        context;
    private final RequestQueue   requestQueue;

    public RestaurantNetworkRequestManager(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(context/*, new OkHttp3Stack()*/);
    }

    public <T> void addApiRequest(final int method, final String url, final String body, final ResponseListener<String> listener) {
        JsonRequest<T> request = new RestaurantNetworkRequest(context, method, url, body, new Response.Listener() {
            @Override
            public void onResponse(Object response) {
                NetworkResponse networkResponse = (NetworkResponse) response;
                try {
                    String json = new String(networkResponse.data, HttpHeaderParser.parseCharset(networkResponse.headers));
                    listener.onSuccess(json);
                    Log.d("NetworkRequestManager", "Response is:" + json);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                try {
                    Log.e("NetworkRequestManager", "Error network response");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RestaurantError error = ErrorUtils.parseError(context, volleyError);
                listener.onError(error);
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(DEFAULT_TIMEOUT_MS, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        Log.d("NetworkRequestManager","Request is: {}"+ request.toString());
        requestQueue.add(request);
    }

}
