package com.demo.ankurpratik.resturantapp.utils;

/**
 * @author ankurpratik on 11/06/17.
 */
public interface PermissionCallback {
    public void onPermissionGranted(int permissionCode);

    public void onPermissionFailure(int permissionCode);
}