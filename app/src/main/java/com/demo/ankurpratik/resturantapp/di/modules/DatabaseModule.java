package com.demo.ankurpratik.resturantapp.di.modules;

import android.content.Context;

import com.demo.ankurpratik.resturantapp.data.db.BaseDbHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author ankurpratik on 10/06/17.
 */

@Module
public class DatabaseModule {
    private Context context;

    public DatabaseModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public BaseDbHelper providesBaseDBHelper() {
        return new BaseDbHelper(context);
    }
}
