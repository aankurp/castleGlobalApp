package com.demo.ankurpratik.resturantapp.data.db;

import com.demo.ankurpratik.resturantapp.data.dao.BookMarksDao;
import com.demo.ankurpratik.resturantapp.data.dao.SearchItemDao;

/**
 * @author ankurpratik on 10/06/17.
 */

public interface DbTables {
    String   DATABASE_NAME               = "resturant.db";
    int      DATABASE_VERSION            = 1;

    String[] DB_TABLE_NAMES              = new String[] {
            SearchItemDao.TABLE_SEARCH,
            BookMarksDao.TABLE_BOOKMARK
    };

    String[] DB_SQL_CREATE_TABLE_QUERIES = new String[] {
            SearchItemDao.CREATE_TABLE_SEARCH,
            BookMarksDao.CREATE_TABLE_BOOKMARK
    };
}