package com.demo.ankurpratik.resturantapp.utils;

/**
 * @author ankurpratik on 10/06/17.
 */

public interface ResponseListener<T> {
    void onSuccess(T response);

    void onError(RestaurantError error);
}