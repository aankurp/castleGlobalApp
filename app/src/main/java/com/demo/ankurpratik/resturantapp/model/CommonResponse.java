package com.demo.ankurpratik.resturantapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ankurpratik on 10/06/17.
 */

class CommonResponse {
    @SerializedName("results_found")
    @Expose
    private Integer resultsFound;

    @SerializedName("results_start")
    @Expose
    private Integer resultsStart;

    @SerializedName("results_shown")
    @Expose
    private Integer resultsShown;

    public Integer getResultsFound() {
        return resultsFound;
    }

    public void setResultsFound(Integer resultsFound) {
        this.resultsFound = resultsFound;
    }

    public Integer getResultsStart() {
        return resultsStart;
    }

    public void setResultsStart(Integer resultsStart) {
        this.resultsStart = resultsStart;
    }

    public Integer getResultsShown() {
        return resultsShown;
    }

    public void setResultsShown(Integer resultsShown) {
        this.resultsShown = resultsShown;
    }

    @Override
    public String toString() {
        return "CommonResponse{" +
                "resultsFound=" + resultsFound +
                ", resultsStart=" + resultsStart +
                ", resultsShown=" + resultsShown +
                '}';
    }
}
