package com.demo.ankurpratik.resturantapp.data.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.demo.ankurpratik.resturantapp.data.db.BaseDbHelper;
import com.demo.ankurpratik.resturantapp.entity.SearchItemDO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ankurpratik on 10/06/17.
 */

public class SearchItemDao {
    public static final String  TIME                = "time";
    public static final String  TABLE_SEARCH        = "search_table";
    public static final String  ITEM                = "item";
    private static final String _ID                 = "id";
    public static final String  CREATE_TABLE_SEARCH = "CREATE TABLE " + TABLE_SEARCH + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ITEM + " TEXT," + TIME + " TEXT" + ")";
    private static String       TAG                 = SearchItemDao.class.getSimpleName();
    private BaseDbHelper        dbHelper;

    public SearchItemDao(BaseDbHelper dbHelper) {
        this.dbHelper = dbHelper;
        //UniwareApplication.getInstance().getApplicationComponent().inject(this);
    }

    public void insertOrUpdate(String searchText) {
        if (searchText.isEmpty()) {
            return;
        }
        SearchItemDO searchItemDO = getSearchItem(searchText);
        if (searchItemDO == null) {
            insert(searchText);
        } else {
            searchItemDO.setTimeMillis(System.currentTimeMillis());
            update(searchItemDO);
        }
    }

    private void update(SearchItemDO searchItemDO) {
        update(searchItemDO.getSearchText(), String.valueOf(searchItemDO.getTimeMillis()));
    }

    public boolean update(String searchText, String time) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TIME, time);
        contentValues.put(ITEM, searchText);
        Log.d(TAG, "update " + searchText);
        return dbHelper.update(TABLE_SEARCH, contentValues, ITEM + " =?", new String[] { searchText });
    }

    private void insert(String searchText) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(TIME, String.valueOf(System.currentTimeMillis()));
        contentValues.put(ITEM, searchText);
        dbHelper.insert(contentValues, TABLE_SEARCH);
        Log.d(TAG, "insert " + searchText);
    }

    public SearchItemDO getSearchItem(String searchText) {
        SearchItemDO searchItemDO = null;
        Cursor cursor = null;
        try {
            cursor = dbHelper.getReadableDatabase().query(TABLE_SEARCH, null, ITEM + "=? ", new String[] { searchText }, null, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    searchItemDO = new SearchItemDO();
                    searchItemDO.setSearchText(cursor.getString(cursor.getColumnIndex(ITEM)));
                    searchItemDO.setTimeMillis(Long.parseLong(cursor.getString(cursor.getColumnIndex(TIME))));

                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.d(TAG, "getSearchItem " + searchItemDO);
        return searchItemDO;
    }

    public List<SearchItemDO> getAllSearchItems() {
        List<SearchItemDO> searchItemDOs = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = dbHelper.getReadableDatabase().query(TABLE_SEARCH, null, null, null, null, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    SearchItemDO searchItemDO = new SearchItemDO();
                    searchItemDO.setSearchText(cursor.getString(cursor.getColumnIndex(ITEM)));
                    searchItemDO.setTimeMillis(Long.parseLong(cursor.getString(cursor.getColumnIndex(TIME))));
                    searchItemDOs.add(searchItemDO);
                } while (cursor.moveToNext());

            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.d(TAG, "getAllSearchItems " + searchItemDOs);
        return searchItemDOs;
    }
}
