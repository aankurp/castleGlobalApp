package com.demo.ankurpratik.resturantapp.di.modules;

import android.content.Context;

import com.demo.ankurpratik.resturantapp.RestaurantApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author ankurpratik on 10/06/17.
 */

@Module
public class ApplicationModule {
    RestaurantApplication application;

    public ApplicationModule(RestaurantApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    RestaurantApplication providesApplication() {
        return application;
    }

    @Provides
    @Singleton
    Context providesApplicationContext() {
        return application;
    }
}
