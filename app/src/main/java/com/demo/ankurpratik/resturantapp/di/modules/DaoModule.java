package com.demo.ankurpratik.resturantapp.di.modules;

import android.content.Context;

import com.demo.ankurpratik.resturantapp.data.dao.BookMarksDao;
import com.demo.ankurpratik.resturantapp.data.dao.SearchItemDao;
import com.demo.ankurpratik.resturantapp.data.db.BaseDbHelper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author ankurpratik on 10/06/17.
 */
@Module
public class DaoModule {
    private Context context;

    public DaoModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public SearchItemDao providesSearchItemDao(BaseDbHelper baseDbHelper) {
        return new SearchItemDao(baseDbHelper);
    }

    @Provides
    @Singleton
    public BookMarksDao providesBookMarkDao(BaseDbHelper baseDbHelper) {
        return new BookMarksDao(baseDbHelper);
    }
}
