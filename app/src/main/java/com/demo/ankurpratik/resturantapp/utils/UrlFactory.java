package com.demo.ankurpratik.resturantapp.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ankurpratik on 10/06/17.
 */

public class UrlFactory {
    public static final int SEARCH_ITEM = 0;
    public static String    domain      = "https://developers.zomato.com/api/v2.1/";

    private UrlFactory() {

    }

    public static String getUrl(int type) {
        Map<String, String> map = new HashMap<String, String>();
        return getUrl(type, map);
    }

    public static String getUrl(int type, Map<String, String> map) {
        switch (type) {
            case SEARCH_ITEM:
                String url = domain + "search";
                if (map.get("search") != null) {
                    url = url + "?q=" + map.get("search");
                }
                if (map.get("sortType") != null) {
                    url = url + "&sort=" + map.get("sortType");
                }
                return url;
        }
        return null;
    }
}
