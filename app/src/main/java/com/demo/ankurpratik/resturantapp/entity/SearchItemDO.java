package com.demo.ankurpratik.resturantapp.entity;

/**
 * @author ankurpratik on 10/06/17.
 */

public class SearchItemDO {
    private String searchText;
    private long timeMillis;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public long getTimeMillis() {
        return timeMillis;
    }

    public void setTimeMillis(long timeMillis) {
        this.timeMillis = timeMillis;
    }

    @Override
    public String toString() {
        return "SearchItemDO{" +
                "searchText='" + searchText + '\'' +
                ", timeMillis=" + timeMillis +
                '}';
    }
}
