package com.demo.ankurpratik.resturantapp.data.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author ankurpratik on 10/06/17.
 */

public class BaseDbHelper extends SQLiteOpenHelper implements DbTables {

    private static final String TAG = "BaseDbHelper";
    private SQLiteDatabase db = null;

    public BaseDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String DB_SQL_CREATE_TABLE_QUERY : DB_SQL_CREATE_TABLE_QUERIES) {
            db.execSQL(DB_SQL_CREATE_TABLE_QUERY);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (String DB_TABLE_NAME : DB_TABLE_NAMES) {
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE_NAME);
        }
        // TODO: 07/06/17 decide what to do when app updates
        onCreate(db);
    }

    private synchronized SQLiteDatabase getDatabase(){
        if(db == null){
            db = this.getWritableDatabase();
        }
        return db;
    }

    public synchronized boolean insert(ContentValues values, String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(tableName, null, values);
        return true;
    }

    public synchronized boolean update(String tableName, ContentValues values, String whereClause, String[] whereAgs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(tableName, values, whereClause, whereAgs);
        return true;
    }

    public synchronized void delete(String tableName, String whereClause, String[] whereAgs) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(tableName, whereClause, whereAgs);
    }

    public Cursor executeQuery(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
    }

    public Cursor executeRawQuery(String sql, String[] selectionArgs) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.rawQuery(sql, selectionArgs);
    }

    public boolean insertOrUpdate(ContentValues values, String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        return true;
    }

    public synchronized boolean removeTableItems(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + tableName);
        return true;
    }

    public synchronized boolean restartTable(String tableName, String createTableQuery) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + tableName);
        db.execSQL(createTableQuery);
        return true;
    }

    public int getCount(String tableName) {
        int count = 0;
        String countQuery = "SELECT  * FROM " + tableName;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();
        return count;
    }
}
