package com.demo.ankurpratik.resturantapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.demo.ankurpratik.resturantapp.R;
import com.demo.ankurpratik.resturantapp.entity.BookMarksDO;
import com.demo.ankurpratik.resturantapp.model.Restaurant;
import com.demo.ankurpratik.resturantapp.utils.RecycleViewItemClick;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ankurpratik on 10/06/17.
 */

public class BookMarksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<BookMarksDO> bookMarksDOs = new ArrayList<>();
    private Context           context;
    private RecycleViewItemClick recycleViewItemClick;

    public BookMarksAdapter(Context context, RecycleViewItemClick recycleViewItemClick, List<BookMarksDO> list) {
        this.context = context;
        this.recycleViewItemClick = recycleViewItemClick;
        setData(list);
    }

    public void setData(List<BookMarksDO> list) {
        bookMarksDOs.clear();
        bookMarksDOs.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View childView = inflater.inflate(R.layout.restaurant_list_layout, parent, false);
        viewHolder = new ChildViewHolder(childView);
        childView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recycleViewItemClick.onRecyclerItemClick(v);
            }
        });
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ChildViewHolder vh2 = (ChildViewHolder) holder;
        configureChild(vh2, position);
    }

    private void configureChild(ChildViewHolder vh2, int position) {
        BookMarksDO bookMarksDO = bookMarksDOs.get(position);
        if (bookMarksDO != null) {
            vh2.getId().setText(bookMarksDO.getRestaurantId());
            vh2.getAddress().setText(bookMarksDO.getAddress());
            vh2.getName().setText(bookMarksDO.getRestaurantName());
            vh2.getRating().setText(bookMarksDO.getRatingText());
            vh2.getPrice().setText("Price Range: " + bookMarksDO.getPriceText());
            if (bookMarksDO.getThumbUrl() != null && !bookMarksDO.getThumbUrl().isEmpty()) {
                Glide.with(context).load(bookMarksDO.getThumbUrl()).thumbnail(0.5f).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(vh2.getRestaurantImage());
            } else {
                Glide.clear(vh2.getRestaurantImage());
                vh2.getRestaurantImage().setImageDrawable(null);
            }
        }
    }

    @Override
    public int getItemCount() {
        return bookMarksDOs.size();
    }

    public BookMarksDO getBookMarkDO(int position) {
        return bookMarksDOs.get(position);
    }

    public void removeData(int position) {
        bookMarksDOs.remove(position);
    }
}
