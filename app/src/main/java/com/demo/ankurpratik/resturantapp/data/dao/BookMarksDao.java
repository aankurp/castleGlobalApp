package com.demo.ankurpratik.resturantapp.data.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import com.demo.ankurpratik.resturantapp.data.db.BaseDbHelper;
import com.demo.ankurpratik.resturantapp.entity.BookMarksDO;
import com.demo.ankurpratik.resturantapp.model.Restaurant;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ankurpratik on 10/06/17.
 */

public class BookMarksDao {
    public static final String  TABLE_BOOKMARK        = "bookmark_table";
    public static final String  PRICE_TEXT            = "price";
    public static final String  THUMB_URL             = "thumb_url";
    private static final String _ID                   = "id";
    private static final String RESTAURANT_ID         = "restaurant_id";
    private static final String RESTAURANT_NAME       = "restaurant_name";
    private static final String RATING_TEXT           = "rating_text";
    private static final String ADDRESS               = "address";
    public static final String  CREATE_TABLE_BOOKMARK = "CREATE TABLE " + TABLE_BOOKMARK + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + PRICE_TEXT + " TEXT," + THUMB_URL
            + " TEXT," + RESTAURANT_ID + " TEXT," + RESTAURANT_NAME + " TEXT," + RATING_TEXT + " TEXT," + ADDRESS + " TEXT" + ")";
    private static String       TAG                   = BookMarksDao.class.getSimpleName();
    private BaseDbHelper        dbHelper;

    public BookMarksDao(BaseDbHelper dbHelper) {
        this.dbHelper = dbHelper;
        //UniwareApplication.getInstance().getApplicationComponent().inject(this);
    }

    public void insertBookMark(BookMarksDO bookMarksDO) {
        insert(bookMarksDO);
    }

    private void insert(BookMarksDO bookMarksDO) {
        insert(bookMarksDO.getPriceText(), bookMarksDO.getRestaurantId(), bookMarksDO.getAddress(), bookMarksDO.getRatingText(), bookMarksDO.getThumbUrl(),
                bookMarksDO.getRestaurantName());
    }

    public void deleteBookMark(BookMarksDO bookMarksDO) {
        dbHelper.delete(TABLE_BOOKMARK, RESTAURANT_ID + " = ?", new String[] { String.valueOf(bookMarksDO.getRestaurantId()) });
        Log.d(TAG, "deleteBookMark bookMarksDO " + bookMarksDO);
    }

    public void insert(Restaurant restaurant) {
        insert(String.valueOf(restaurant.getRestaurant().getPriceRange()), restaurant.getRestaurant().getId(), restaurant.getRestaurant().getLocation().getAddress(),
                restaurant.getRestaurant().getUserRating().getRatingText(), restaurant.getRestaurant().getThumb(), restaurant.getRestaurant().getName());
    }

    public void insert(String priceText, String restaurantId, String address, String ratingText, String thumb, String restaurantName) {
        BookMarksDO bookMarksDO = getBookMarkedRestaurant(restaurantId);
        if (bookMarksDO == null) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(PRICE_TEXT, priceText);
            contentValues.put(RESTAURANT_ID, restaurantId);
            contentValues.put(RESTAURANT_NAME, restaurantName);
            contentValues.put(ADDRESS, address);
            contentValues.put(RATING_TEXT, ratingText);
            contentValues.put(THUMB_URL, thumb);
            dbHelper.insert(contentValues, TABLE_BOOKMARK);
            Log.d(TAG, "insert priceText " + priceText + " restaurantId " + restaurantId + " address " + address + " ratingText " + ratingText + " thumb " + " restaurantName "
                    + restaurantName);
        }
    }

    private BookMarksDO getBookMarkedRestaurant(String restaurantId) {
        BookMarksDO bookMarksDO = null;
        Cursor cursor = null;
        try {
            cursor = dbHelper.getReadableDatabase().query(TABLE_BOOKMARK, null, RESTAURANT_ID + "=? ", new String[] { restaurantId }, null, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    bookMarksDO = new BookMarksDO();
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.d(TAG, "getBookMarkedRestaurant " + bookMarksDO);
        return bookMarksDO;
    }

    public List<BookMarksDO> getAllBookMarks() {
        List<BookMarksDO> bookMarksDOs = new ArrayList<>();
        Cursor cursor = null;
        try {
            cursor = dbHelper.getReadableDatabase().query(TABLE_BOOKMARK, null, null, null, null, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    BookMarksDO bookMarksDO = new BookMarksDO();
                    bookMarksDO.setAddress(cursor.getString(cursor.getColumnIndex(ADDRESS)));
                    bookMarksDO.setPriceText((cursor.getString(cursor.getColumnIndex(PRICE_TEXT))));
                    bookMarksDO.setRatingText(cursor.getString(cursor.getColumnIndex(RATING_TEXT)));
                    bookMarksDO.setRestaurantId(cursor.getString(cursor.getColumnIndex(RESTAURANT_ID)));
                    bookMarksDO.setRestaurantName(cursor.getString(cursor.getColumnIndex(RESTAURANT_NAME)));
                    bookMarksDO.setThumbUrl(cursor.getString(cursor.getColumnIndex(THUMB_URL)));
                    bookMarksDOs.add(bookMarksDO);
                } while (cursor.moveToNext());

            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Log.d(TAG, "getAllBookMarks " + bookMarksDOs);
        return bookMarksDOs;
    }

    public boolean isRestaurantBookMarked(String restaurantId) {
        if(getBookMarkedRestaurant(restaurantId) != null){
            return true;
        } else {
            return false;
        }
    }
}
