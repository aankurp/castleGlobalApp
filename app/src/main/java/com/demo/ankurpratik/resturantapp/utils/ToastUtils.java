package com.demo.ankurpratik.resturantapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author ankurpratik on 10/06/17.
 */

public class ToastUtils {
    private ToastUtils() {
        //Empty
    }

    public static void showErrorMessage(final Context context, final String message) {
        Toast toast = getToast(context, message);
        View view = toast.getView();
        view.setBackgroundColor(context.getResources().getColor(android.R.color.holo_red_light));
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTypeface(null, Typeface.BOLD);
        text.setGravity(Gravity.CENTER_HORIZONTAL);
        toast.show();
    }

    public static void showInfoMessage(Context context, String message) {
        Toast toast = getToast(context, message, Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundColor(context.getResources().getColor(android.R.color.holo_blue_light));
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTypeface(null, Typeface.BOLD);
        text.setGravity(Gravity.CENTER_HORIZONTAL);
        toast.show();
    }

    public static void showSuccessMessage(Context context, String message) {
        Toast toast = getToast(context, message, Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundColor(context.getResources().getColor(android.R.color.holo_green_light));
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTypeface(null, Typeface.BOLD);
        text.setGravity(Gravity.CENTER_HORIZONTAL);
        toast.show();
    }

    private static Toast getToast(Context context, String message) {
        return getToast(context, message, Toast.LENGTH_LONG);
    }

    private static Toast getToast(Context context, String message, int length) {
        return Toast.makeText(context, message, length);
    }

    private static LinearLayout.LayoutParams getLayoutParams(){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(5, 2, 5, 2);
        return params;
    }
}
