package com.demo.ankurpratik.resturantapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RestaurantResponse extends CommonResponse {
    @SerializedName("restaurants")
    @Expose
    private List<Restaurant> restaurants = new ArrayList<>();

    public List<Restaurant> getRestaurants() {
        return restaurants;
    }

    public void setRestaurants(List<Restaurant> restaurants) {
        this.restaurants = restaurants;
    }

    @Override
    public String toString() {
        return "RestaurantResponse{" +
                "restaurants=" + restaurants +
                '}';
    }
}
