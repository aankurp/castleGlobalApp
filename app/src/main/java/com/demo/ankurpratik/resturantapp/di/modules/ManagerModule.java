package com.demo.ankurpratik.resturantapp.di.modules;

import android.content.Context;

import com.demo.ankurpratik.resturantapp.managers.PermissionManager;
import com.demo.ankurpratik.resturantapp.managers.RestaurantManager;
import com.demo.ankurpratik.resturantapp.network.RestaurantNetworkRequestManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * @author ankurpratik on 10/06/17.
 */

@Module
public class ManagerModule {
    private Context context;

    public ManagerModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public RestaurantManager providesRestaurantManager() {
        return new RestaurantManager(context);
    }

    @Provides
    @Singleton
    public RestaurantNetworkRequestManager providesRestaurantNetworkRequestManager() {
        return new RestaurantNetworkRequestManager(context);
    }

    @Provides
    @Singleton
    public PermissionManager providesPermissionManager() {
        return new PermissionManager(context);
    }
}
