package com.demo.ankurpratik.resturantapp.ui.activity;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.demo.ankurpratik.resturantapp.R;
import com.demo.ankurpratik.resturantapp.enums.FragmentType;
import com.demo.ankurpratik.resturantapp.managers.RestaurantManager;
import com.demo.ankurpratik.resturantapp.ui.fragment.RestaurantListFragment;
import com.demo.ankurpratik.resturantapp.ui.fragment.BookMarksFragment;
import com.demo.ankurpratik.resturantapp.utils.FragmentChanger;

import javax.inject.Inject;

public class RestaurantActivity extends AppCompatActivity implements FragmentChanger {
    public static final int LIST_FRAGMENT     = 0;
    public static final int BOOKMARK_FRAGMENT = 1;
    @Inject
    RestaurantManager       restaurantManager;
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            setFragment(FragmentType.RESTAURANT_LIST);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void changeFragment() {

    }

    @Override
    public void setFragment(FragmentType tag, Bundle bundle) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = null;
        String fragmentTag = null;
        boolean addToBackStackFlag = true;
        switch (tag) {
            case RESTAURANT_LIST:
                fragment = RestaurantListFragment.newInstance();
                fragmentTag = RestaurantListFragment.class.getSimpleName();
                fragment.setArguments(bundle);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                fragmentTransaction.add(R.id.fragment_container, fragment, fragmentTag).commit();
                break;
            case BOOKMARK:
                fragment = BookMarksFragment.newInstance();
                fragmentTag = BookMarksFragment.class.getSimpleName();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void setFragment(FragmentType tag) {
        Bundle bundle = new Bundle();
        setFragment(tag, bundle);
    }
}
