package com.demo.ankurpratik.resturantapp.adapters;

import android.app.Application;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.demo.ankurpratik.resturantapp.R;
import com.demo.ankurpratik.resturantapp.RestaurantApplication;
import com.demo.ankurpratik.resturantapp.managers.RestaurantManager;
import com.demo.ankurpratik.resturantapp.model.Restaurant;
import com.demo.ankurpratik.resturantapp.model.Restaurant_;
import com.demo.ankurpratik.resturantapp.utils.RecycleViewItemClick;
import com.demo.ankurpratik.resturantapp.utils.ResponseListener;
import com.demo.ankurpratik.resturantapp.utils.RestaurantError;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RestaurantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int            HEADER      = 0, CHILD = 1;
    @Inject
    public RestaurantManager     restaurantManager;
    private List<Restaurant>     restaurants = new ArrayList<>();
    private Context              context;
    private RecycleViewItemClick recycleViewItemClick;

    public RestaurantAdapter(Context context, RecycleViewItemClick recycleViewItemClick, Map<String, List<Restaurant>> map) {
        this.context = context;
        this.recycleViewItemClick = recycleViewItemClick;
        setData(map);
        RestaurantApplication.getInstance().getApplicationComponent().inject(this);
    }

    public void setData(Map<String, List<Restaurant>> map) {
        String cuisine;
        restaurants.clear();
        for (Map.Entry<String, List<Restaurant>> entry : map.entrySet()) {
            cuisine = entry.getKey();
            restaurants.add(getHeader(cuisine));
            restaurants.addAll(entry.getValue());
        }
        notifyDataSetChanged();
    }

    private Restaurant getHeader(String cuisine) {
        Restaurant restaurant = new Restaurant();
        restaurant.setRestaurant(new Restaurant_());
        restaurant.getRestaurant().setId("HEADER");
        restaurant.getRestaurant().setCuisines(cuisine);
        return restaurant;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        switch (viewType) {
            case HEADER:
                View headerView = inflater.inflate(R.layout.header_layout, parent, false);
                viewHolder = new HeaderViewHolder(headerView);
                break;
            case CHILD:
                View childView = inflater.inflate(R.layout.restaurant_list_layout, parent, false);
                viewHolder = new ChildViewHolder(childView);
                childView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        recycleViewItemClick.onRecyclerItemClick(v);
                    }
                });
                break;
            default:
                View childView1 = inflater.inflate(R.layout.restaurant_list_layout, parent, false);
                viewHolder = new ChildViewHolder(childView1);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case HEADER:
                HeaderViewHolder vh1 = (HeaderViewHolder) holder;
                configureHeader(vh1, position);
                break;
            case CHILD:
                ChildViewHolder vh2 = (ChildViewHolder) holder;
                configureChild(vh2, position);
                break;
            default:
                ChildViewHolder vh3 = (ChildViewHolder) holder;
                configureChild(vh3, position);
                break;
        }
    }

    private void configureChild(final ChildViewHolder vh2, int position) {
        Restaurant restaurant = restaurants.get(position);
        if (restaurant != null) {
            if (restaurant.getRestaurant().getId() != null)
                vh2.getId().setText(restaurant.getRestaurant().getId());
            if (restaurant.getRestaurant().getLocation() != null)
                vh2.getAddress().setText(restaurant.getRestaurant().getLocation().getAddress());
            if (restaurant.getRestaurant().getName() != null)
                vh2.getName().setText(restaurant.getRestaurant().getName());
            vh2.getRating().setText(restaurant.getRestaurant().getUserRating().getRatingText());
            vh2.getPrice().setText("Price Range: " + String.valueOf(restaurant.getRestaurant().getPriceRange()));
            if (restaurant.getRestaurant().getThumb() != null && !restaurant.getRestaurant().getThumb().isEmpty()) {
                Glide.with(context).load(restaurant.getRestaurant().getThumb()).thumbnail(0.5f).crossFade().diskCacheStrategy(DiskCacheStrategy.ALL).into(vh2.getRestaurantImage());
            } else {
                Glide.clear(vh2.getRestaurantImage());
                vh2.getRestaurantImage().setImageDrawable(null);
            }
            restaurantManager.isRestaurantBookMarked(restaurant, new ResponseListener<Boolean>() {
                @Override
                public void onSuccess(Boolean response) {
                    if(response){
                        vh2.getBookMarkLayout().setVisibility(View.VISIBLE);
                    } else{
                        vh2.getBookMarkLayout().setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError(RestaurantError error) {

                }
            });
        }
    }

    private void configureHeader(HeaderViewHolder vh1, int position) {
        Restaurant restaurant = restaurants.get(position);
        if (restaurant != null) {
            vh1.getCuisineTextView().setText(restaurant.getRestaurant().getCuisines());
        }
    }

    @Override
    public int getItemCount() {
        return restaurants.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (restaurants.get(position).getRestaurant() != null && restaurants.get(position).getRestaurant().getId().equals("HEADER")) {
            return HEADER;
        } else {
            return CHILD;
        }
    }

    public Restaurant getRestaurant(int position) {
        return restaurants.get(position);
    }
}
