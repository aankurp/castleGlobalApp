package com.demo.ankurpratik.resturantapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.demo.ankurpratik.resturantapp.R;

/**
 * @author ankurpratik on 10/06/17.
 */

public class HeaderViewHolder extends RecyclerView.ViewHolder {

    private TextView cuisineText;

    public HeaderViewHolder(View v) {
        super(v);
        cuisineText = (TextView) v.findViewById(R.id.cuisine);
    }

    public TextView getCuisineTextView() {
        return cuisineText;
    }

    public void setCuisneText(TextView ivExample) {
        this.cuisineText = ivExample;
    }
}
