package com.demo.ankurpratik.resturantapp.network;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import android.content.Context;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RestaurantNetworkRequest<T> extends JsonRequest<T> {

    private final Context context;

    public RestaurantNetworkRequest(Context context, int method,
                          String url, String requestBody, Response.Listener<T> listener,
                          Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        this.context = context;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        headers.put("user-key", "0ec5ea2a25721cee83ceea7ad9aa7fda");
        return headers;
    }

    @Override
    protected Response parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            return Response.success(response,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        }
    }
}