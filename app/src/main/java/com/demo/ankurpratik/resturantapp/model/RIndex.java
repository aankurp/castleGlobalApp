package com.demo.ankurpratik.resturantapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author ankurpratik on 10/06/17.
 */

public class RIndex {
    @SerializedName("res_id")
    @Expose
    private Integer resId;

    public Integer getResId() {
        return resId;
    }

    public void setResId(Integer resId) {
        this.resId = resId;
    }

    @Override
    public String toString() {
        return "RIndex{" +
                "resId=" + resId +
                '}';
    }
}